Getting started
===============

This is where you describe how to get set up on a clean install, including the
commands necessary to get the raw data, and then how to make the cleaned, final data sets.

To contribute to this project on a Windows machine, the following are required:

*   LabVIEW (to generate data)

*   PowerShell (shell)

*   Git (version control)

*   Python 2.7 (to run data acquisition software)

*   pip (to install python packages, eg sphinx)

*   sphinx (to compile documentation)

Documentation
-------------

This is stored in in ``\docs`` and is compiled into a single html page from several .rst files using sphinx.

To compile the documentation in PowerShell, run the command ``.\make.bat singlehtml`` from ``\docs``.
