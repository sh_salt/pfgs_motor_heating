.. PFGS Motor Heating documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PFGS Motor Heating documentation
==============================================

Contents:

.. toctree::
   :maxdepth: 2

   getting-started
   .. commands // removed until commands are added
   procedure



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
