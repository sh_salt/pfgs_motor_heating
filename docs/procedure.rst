Procedure
=========

Aim
---

To characterise the motor heating in two stepper motors that will be used on the new Prime Focus Guidance System.

Driving System Limitation
-------------------------

``SALT1000AS1000_A SALT Design Principles -  2.5.2.e General design guidelines and constraints``

``SALT1524AS1002_B  Specification, Upgrade, PF Guidance System - 5.2.2.2 Thermal``

The motor casings must not increase in temperature beyond 2 degrees Celsius above ambient temperature.

Assumptions
-----------

The follow conditions are assumed:

*   The motor is initially at ambient temperature.

*   The ambient temperature stays constant throughout the experiment.

Hypothesis
----------

The motor will heat up under operation, the significance of this heating effect is not yet known.

Data Acquisition
----------------

1.  From the ELS-WebViewer, acquire pre-recorded guider movements located from::

        tpc / guidance_system_status / probe_x / 1
        tpc / guidance_system_status / probe_y / 1

    Download a .zip file containing the data in .csv format by clicking the download button on the page.

2.  Extract the .zip file and place the .csv files in ``\data\external\movements``

3.  Open and run the LabVIEW project in ``\src\data`` to simulate the guider movements on the stepper motor. Use the downloaded .csv files as the movement inputs. Select which day of data will be run, day 0 is the first day in the data file.

4.  Position the infrared thermometer to detect the heat emitted by the motor, record this using the OCR software. Save the resulting .csv file into ``\data\raw``. Record the motor movements and the times they occur for comparison.

5.  The test can be stopped when the movements are complete and the motor has returned to ambient temperature.

Data Processing
---------------

6.  From the test root directory ``\``:

    6.1 To reduce the data file, run the python script at ``.\src\data\process_data.py``. Include the -s flag to save the result. The reduced data can be found in ``\src\data\processed\``.

    6.2 Run the python script at ``.\src\visualization\visualize.py``. Include the -s flag to save the result. The graphs can be found in ``\reports\figures\``.

7.  Write the scripts required to do the processing below.

    7.1 Apply a basic high-pass filter to remove long term temperature effects. This should also indicate an offset from the initial ambient temperature.

    7.2 Correlate the motor movement data with the temperature data, plotting both on the same graph.

Report Requirements
-------------------

8.  Write a short one-page report using the SALT template detailing the results of the characterisation. Include a recommendation of proceeding with the current proposed system if the heating specification is met, or suggest an alternative to
