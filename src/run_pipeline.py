"""Usage:
    run_pipeline.py [<meer_name>] [-pv]
    run_pipeline.py (-h | --help)

   Arguments:
    [<meer_name>]       The .meer.json file of interest

   Options:
    -p --plot-only      Re-plot the graphs, do not recompile data
    -v --verbose        Print data summary between steps
    -h --help           Show this help
"""
from docopt import docopt
from os import path, system
import json

config_file = path.join('src', 'config', 'data_files.json')


def load_files(config_file):
    # load config file
    with open(config_file, 'r') as f:
        try:
            file_list = json.load(f)['file_list']

        # if the file is empty the ValueError will be thrown
        except ValueError:
            print("ERROR: {} is an invalid json file".format(config_file))

    return file_list


def execute_scripts(file_list, plot_only):
    for fl in file_list:
        print("Pipelining {}...".format(fl['meer_name']))

        if plot_only is False:
            system("python {script} to {meer_name} {path_temp} {path_move}".format(
                script=path.join('src', 'data', 'process_data.py'),
                meer_name=fl['meer_name'],
                path_temp=path.join('data', 'raw', fl['file_temp']),
                path_move=path.join('data', 'raw', fl['file_move']),
            ))
            system("python {script} {meer_path}".format(
                script=path.join('src', 'data', 'analyse_data.py'),
                meer_path=path.join('data', 'processed',
                                    fl['meer_name'] + ".meer.json"),
            ))
        system("python {script} {meer_path}".format(
            script=path.join('src', 'visualization', 'processed_visualise.py'),
            meer_path=path.join('data', 'processed',
                                fl['meer_name'] + ".meer.json"),
        ))
        system("python {script} {meer_path}".format(
            script=path.join('src', 'visualization', 'analysed_visualise.py'),
            meer_path=path.join('data', 'analysed',
                                fl['meer_name'] + ".meer.json"),
        ))

if __name__ == '__main__':
    args = docopt(__doc__)
    # print(args)

    if args['--verbose']:
        VERBOSE = 1

    meer_name = args['<meer_name>']
    plot_only = args['--plot-only']

    try:
        file_list = load_files(config_file)

        if meer_name:
            for fl in file_list:
                if fl['meer_name'] == meer_name:
                    execute_scripts([fl], plot_only)
        else:
            execute_scripts(file_list, plot_only)

        print("Successfully completed pipeline")
    except Exception as e:
        print("Pipeline failed")
        print(str(e))
        import traceback
        traceback.print_exc()
