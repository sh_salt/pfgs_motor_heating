"""Usage:
    analysed_visualise.py <path_meer> [-vs]
    analysed_visualise.py (-h | --help)

   Arguments:
    <path_meer>         Path to the .meer.json file of interest

   Options:
    -v --verbose        Print data summary between steps
    -h --help           Show this help
"""
import sys
from os import path
sys.path.append(path.join(path.dirname(__file__), '..'))

from docopt import docopt
from toolbox import leaves as lv
from toolbox import meerkat as mk

VERBOSE = 0

if __name__ == '__main__':
    args = docopt(__doc__)
    # print(args)

    if args['--verbose']:
        VERBOSE = 1

    lv.set_VERBOSE(VERBOSE)

    path_meer = args['<path_meer>']

    dp = mk.load(path_meer)

    path_plot = path.join(
        'reports',
        'figures',
        "analysed_{}.png".format(path.basename(path_meer).split('.')[0]),
    )

    lv.plot_data(dp, path_plot)

    print("Successfully completed analysis plot")
