"""Usage:
    process_calibration.py <path_meer> [-vV]
    process_calibration.py (-h | --help)

   Arguments:
    <path_meer>         Input .meer.json path

   Options:
    -v --verbose        Print data summary between steps
    -V --very-verbose   Show plots and data summary between steps
    -h --help           Show this help
"""
import sys
from os import path
sys.path.append(path.join(path.dirname(__file__), '..'))

from docopt import docopt
from os import path
from toolbox import leaves as lv
from toolbox import meerkat as mk
import json

VERBOSE = 0


@lv.verbosity("Writing calibration...")
def write_calibration(file_name, motor_type, error):
    # save as json
    path_calibr = path.join('src', 'config', "{filename}.json".format(
        filename=file_name,
    ))

    with open(path_calibr, 'r') as f:
        try:
            calibration = json.load(f)

        # if the file is empty the ValueError will be thrown
        except ValueError:
            print("ERROR: {} is an invalid json file".format(path_calibr))

    with open(path_calibr, 'w') as f:
        calibration['calibration_error'][motor_type] = error
        json.dump(calibration, f)

if __name__ == '__main__':
    args = docopt(__doc__)
    # print(args)

    # Set verbosity settings
    if args['--very-verbose']:
        VERBOSE = 2
    elif args['--verbose']:
        VERBOSE = 1

    lv.set_VERBOSE(VERBOSE)

    # temperature data
    path_meer = args['<path_meer>']
    dp = mk.load(path_meer)

    df_temp = dp.data['temp']

    # get motor type
    motor_type = dp.headers['motor_type']

    # save reduced temp data
    write_calibration("calibration", motor_type, df_temp['delta'].std())

    # Print out final summary
    if VERBOSE == 1:
        lv.summarise(df_temp)

    print("Successfully completed calibration")
