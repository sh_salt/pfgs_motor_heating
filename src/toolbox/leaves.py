from os import path
from pandas import DataFrame, Timedelta
import json
import matplotlib.dates as dates
import matplotlib.pyplot as plt

VERBOSE = 0


def set_VERBOSE(v):
    global VERBOSE
    VERBOSE = v


def summarise(df):
    if type(df) is DataFrame:
        df.plot()


def verbosity(message):
    def func_deco(func):
        def inner(*args, **kwargs):
            if VERBOSE >= 1:
                print(message)

            ret = func(*args, **kwargs)

            if VERBOSE >= 1:
                print("Complete")

            if VERBOSE == 2:
                summarise(ret)

            return ret
        return inner
    return func_deco


def get_calibration():
    path_calibr = path.join('src', 'config', 'calibration.json')
    # load calibration file
    with open(path_calibr, 'r') as f:
        try:
            calibration = json.load(f)

        # if the file is empty the ValueError will be thrown
        except ValueError:
            print("ERROR: {} is an invalid json file".format(path_calibr))

    return calibration


@verbosity("Plotting data...")
def plot_data(data_pack, path_plot):
    column_data = {
        'motor': {
            'label': "Motor",
            'color': "red",
            'alpha': 1.0,
        },
        'motor_error': {
            'label': "Motor Uncertainty (3 std)",
            'color': "red",
            'alpha': 0.2,
        },
        'ambient': {
            'label': "Ambient",
            'color': "green",
            'alpha': 1.0,
        },
        'ambient_error': {
            'label': "Ambient Uncertainty (3 std)",
            'color': "green",
            'alpha': 0.2,
        },
        'delta': {
            'label': "delta Temp",
            'color': "orange",
            'alpha': 1.0,
        },
        'delta_error': {
            'label': "delta Temp Uncertainty (3 std)",
            'color': "orange",
            'alpha': 0.2,
        },
        'movement': {
            'label': "Movement",
            'color': "cyan",
            'alpha': 1.0,
        },
    }

    # Axis Label Strings
    temp_axis_labels = ["Temperatures (*C)", "Delta above ambient (*C)"]
    move_axis_label = "Movement (mm)"
    time_axis_label = "Time Stamp"

    def fix_axis(*plot_nums):
        # Set axis limits
        for i in plot_nums:
            bottom, top = plots[i].get_ylim()
            top = 2 if top < 2 else top
            plots[i].set_ylim(ymin=-0.5*top, ymax=top)

    fig = plt.figure(1)
    plots = []
    plots.append(fig.add_subplot(111))

    # Add movement plot if required
    if 'move' in data_pack.data:
        plots.append(plots[0].twinx())
        plots[1].set_ylabel(move_axis_label)

    # X-axis labels
    plots[0].set_xlabel(time_axis_label)

    # Plot the Data
    for key, df in data_pack.data.items():
        for c in df:
            if c in ['motor', 'ambient', 'delta']:
                plots[0].plot(
                    df.index,
                    df[c],
                    color=column_data[c]['color'],
                    label=column_data[c]['label'],
                )

                ce = '{}_error'.format(c)

                error_top = df[c] + 3*df[ce]
                error_bot = df[c] - 3*df[ce]

                plots[0].fill_between(
                    df.index,
                    error_top,
                    error_bot,
                    color=column_data[ce]['color'],
                    alpha=column_data[ce]['alpha'],
                    label=column_data[ce]['label'],
                )
            elif c in ['movement']:
                plots[1].stem(
                    df.index,
                    df,
                    markerfmt=column_data[c]['color'][0]+'.',
                    linefmt=column_data[c]['color'][0]+'-',
                    basefmt='k:',
                    label=column_data[c]['label'],
                )

    # Set graph type
    if 'move' in data_pack.data:  # temperature and movement
        if 'delta' in data_pack.data['temp']:  # analysed graph
            plt.title(
                "{motor} Heating: {date}".format(
                    motor=data_pack.headers['motor_type'],
                    date=data_pack.headers['date_test'],
                )
            )
            plots[0].set_ylabel(temp_axis_labels[1])
            fix_axis(0, 1)
        else:  # processed graph
            plt.title(
                "{motor} Heating: {date}".format(
                    motor=data_pack.headers['motor_type'],
                    date=data_pack.headers['date_test'],
                )
            )
            plots[0].set_ylabel(temp_axis_labels[0])
            fix_axis(1)

    else:  # temperature only
        plt.title(
            "Instrument Calibration {motor}: {date}".format(
                motor=data_pack.headers['motor_type'],
                date=data_pack.headers['date_test'],
            )
        )
        plots[0].set_ylabel(temp_axis_labels[0])

    # Set major axis
    xax = plots[0].get_xaxis()  # get the x-axis
    date_range = data_pack.data['temp'].index[-1] \
        - data_pack.data['temp'].index[0]

    if date_range > Timedelta(hours=12):
        xax.set_major_locator(dates.HourLocator(byhour=range(0, 24, 6)))
        xax.set_major_formatter(dates.DateFormatter('%H:%M'))
    else:
        xax.set_major_locator(dates.HourLocator(byhour=range(0, 24, 1)))
        xax.set_major_formatter(dates.DateFormatter('%H:%M'))

    # Format X-Axis date
    fig.autofmt_xdate()

    # Show grid-lines
    plots[0].xaxis.grid(True)

    # Shrink plot height
    box = plots[0].get_position()
    box_position = [
        box.x0,
        box.y0 + box.height * 0.1,
        box.width,
        box.height * 0.9
    ]
    for p in plots:
        p.set_position(box_position)

    # Add Legend
    h_t, l_t = plots[0].get_legend_handles_labels()
    for p in plots[1:]:
        h, l = p.get_legend_handles_labels()
        h_t = h_t + h
        l_t = l_t + l

    lgd = plots[0].legend(
        h_t,
        l_t,
        loc='upper center',
        bbox_to_anchor=(0.5, -0.1),
        ncol=2,
    )

    # plt.tight_layout()

    # Save graph as .png
    plt.savefig(path_plot, bbox_extra_artists=(lgd,), bbox_inches='tight')

    if VERBOSE >= 1:
        plt.show()
