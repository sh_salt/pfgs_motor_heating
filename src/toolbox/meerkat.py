import pandas as pd
from os import path
import json


def _drop_zero_columns(df):
    for c in df:
        if df[c].sum() == 0:
            df = df.drop([c], axis=1)

    return df


class DataPack():

    def __init__(self, filename):
        self.filename = filename
        self.data = dict()
        self.headers = dict(data_files=[])

    def add_data(self, df, data_type):
        self.data[data_type] = df
        self.headers['data_files'].append(
            "{data_type}.csv".format(
                data_type=data_type,
            ))

    def trim(self):
        time_buffer = pd.Timedelta(hours=1)
        time_left = self.data.values()[0].index[0]
        time_right = self.data.values()[0].index[-1]
        time_left_buffer = time_left - time_buffer
        time_right_buffer = time_right + time_buffer

        for key, df in self.data.items():
            time_left = max(time_left, df.index[0])
            time_right = min(time_right, df.index[-1])

        for key, df in self.data.items():
            self.data[key] = \
                df[(df.index > time_left_buffer) &
                   (df.index <= time_right_buffer)]

        return self

    def drop_zero_columns(self, key):
        if key is None:
            for key, df in self.data.items():
                self.data[key] = self._drop_zero_columns(df)
        else:
            self.data[key] = _drop_zero_columns(self.data[key])

        return self


def write(dp, file_path):
    # save header as json
    path_json = path.join(file_path, "{filename}.meer.json".format(
        filename=dp.filename,
    ))

    with open(path_json, 'w') as f:
        json.dump(dp.headers, f)

    # save data as csv
    for key in dp.data:
        try:
            dp.data[key].to_csv(
                path.join(file_path, "{filename}.{data_type}.csv".format(
                    filename=dp.filename,
                    data_type=key,
                ))
            )
        except:
            print("WARNING: {} not a DataFrame".format(key))


def load(file_path):
    filename = path.basename(file_path).split('.')[0]

    dp = DataPack(filename)

    # load headers
    with open(file_path, 'r') as f:
        try:
            dp.headers = json.load(f)
        # if the file is empty the ValueError will be thrown
        except ValueError:
            print("ERROR: {} is an invalid .meer file".format(file_path))
            dp.headers = {}

    # load data
    for data_file in dp.headers['data_files']:
        data_type = data_file.split('.')[0]
        path_data = path.join(
            path.dirname(file_path),
            "{}.{}".format(filename, data_file),
            )

        dp.data[data_type] = pd.read_csv(
            path_data,
            parse_dates=True,
            index_col=0,
        )

    return dp
