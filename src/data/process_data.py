"""Usage:
    process_data.py to <filename> [<path_temp>] [<path_move>] [-vV]
    process_data.py (-h | --help)

   Arguments:
    <filename>          Destination filename prefix
    <path_temp>         Input temperature path
    <path_move>         Input movement path

   Options:
    -v --verbose        Print data summary between steps
    -V --very-verbose   Show plots and data summary between steps
    -h --help           Show this help
"""
import sys
from os import path
sys.path.append(path.join(path.dirname(__file__), '..'))

from docopt import docopt
from os import path
from toolbox import leaves as lv
from toolbox import meerkat as mk
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

_plot_num = 0
VERBOSE = 0
motor_types = ["WFS", "Linear"]


@lv.verbosity("Reading file...")
def read_file(filename):

    def read_csv(date_parser):
        dateparse = lambda x: pd.datetime.strptime(x, date_parser)

        if '.temp' in filename:
            col_names = ['timestamp', 'motor', 'ambient', 'dewpoint']
        elif '.move' in filename:
            col_names = ['timestamp', 'motor_type', 'unit', 'value']

        df = pd.read_csv(
            filename,
            skipinitialspace=True,
            parse_dates=[0],
            header=None,
            names=col_names,
            date_parser=dateparse,
        )

        return df

        # 04:07:09.137 PM 8/19/2016
        # or
        # 03:47:12.346 PM 2016/08/16

    try:  # Try the first format
        df = read_csv('%I:%M:%S.%f %p %m/%d/%Y')
    except:  # Try the second format
        df = read_csv('%I:%M:%S.%f %p %Y/%m/%d')

    df = df.set_index(pd.DatetimeIndex(df.timestamp), drop=True, append=False)
    df.index.name = "timestamp"
    df = df.drop(['timestamp'], axis=1, errors='ignore')

    return df


@lv.verbosity("Removing outliers...")
def remove_outliers(df):
    for c in df[:]:
        df[c] = df[c][(df[c] != 0)]  # remove 0 values in rows
        df[c] = df[c][(df[c]-df[c].mean()).abs() <= 5]

    return df


@lv.verbosity("Creating DataPack...")
def create_data_pack(filename, move_src, motor_type, df_temp, df_move):
    dp = mk.DataPack(filename)

    # retrieve the first date out of the move or temp data
    try:
        date_string = ("%r" % df_move.index[0])[11:21]
    except:
        date_string = ("%r" % df_temp.index[0])[11:21]

    dp.headers['date_test'] = date_string

    if move_src is not None:
        dp.headers['move_src'] = move_src

    if motor_type is not None:
        dp.headers['motor_type'] = motor_type

    # store data
    if df_temp is not None:
        dp.add_data(df_temp, 'temp')

    if df_move is not None:
        dp.add_data(df_move, 'move')

    return dp


@lv.verbosity("Saving DataPack...")
def save_data_pack(dp):
    mk.write(dp, path.join('data', 'processed'))


@lv.verbosity("Processing temps...")
def process_data_temp(df_temp):
    calibration = lv.get_calibration()

    def sum_errors(a, b):
        return np.sqrt(a**2 + b**2)

    # remove unwanted data
    df_temp = df_temp.drop(['dewpoint'], axis=1, errors='ignore')
    df_temp = df_temp.groupby(df_temp.index).first()  # remove duplicate inds
    df_temp = remove_outliers(df_temp)
    df_temp = df_temp.dropna(axis='columns', how='all')

    # process remaining data
    #   aggregate and resample
    aggregations = {
        'motor': {
            '': 'mean',
            '_error': 'std',
        },
        'ambient': {
            '': 'mean',
            '_error': 'std',
        },
    }
    df_temp = df_temp.resample('30S', label='left').apply(aggregations)
    df_temp.columns = [''.join(col).strip() for col in df_temp.columns.values]

    #   calculate uncertainties
    sigma_infrared = calibration['infrared_accuracy']
    sigma_air = calibration['air_accuracy']

    uncertain_mean_motor = sigma_infrared
    uncertain_mean_ambient = sigma_air

    df_temp['motor_error'] = sum_errors(
        df_temp['motor_error'],
        uncertain_mean_motor)

    df_temp['ambient_error'] = sum_errors(
        df_temp['ambient_error'],
        uncertain_mean_ambient)

    df_temp = df_temp.replace([np.inf, -np.inf], np.nan)

    df_temp['ambient'] = \
        df_temp['ambient'].rolling(window=5, center=True).mean()

    return df_temp


@lv.verbosity("Processing moves...")
def process_data_move(df_move):
    df_move = df_move[(df_move.value != 0)]  # remove 0 values in rows
    df_move = df_move.assign(movement=df_move['value'].abs())  # add abs col
    df_move = df_move.drop(['value'], axis=1)
    df_move = df_move.resample('60S', label='left').sum()
    df_move = df_move.dropna()

    return df_move


if __name__ == '__main__':
    args = docopt(__doc__)
    # print(args)

    # Set verbosity settings
    if args['--very-verbose']:
        VERBOSE = 2
    elif args['--verbose']:
        VERBOSE = 1

    lv.set_VERBOSE(VERBOSE)

    # Run data merge program
    if args['to']:

        df_temp = None
        df_move = None
        move_src = None
        motor_type = None

        # temperature data
        if args['<path_temp>']:
            df_temp = read_file(args['<path_temp>'])
            df_temp = process_data_temp(df_temp)

            # save reduced temp data
            df_temp.to_csv(args['<path_temp>'].replace('raw', 'interim'))

        # movement data
        if args['<path_move>']:
            df_move = read_file(args['<path_move>'])

            # find move_src
            try:
                move_src = args['<path_temp>'].split("src_", 1)[1][:10]
            except:
                print("WARNING: movements source not found")

            # find motor type out of the move data
            try:
                motor_type = df_move.loc[:, 'motor_type'][0]
            except:
                pass

            # process movement data
            df_move = process_data_move(df_move)

        if motor_type is None:
            for mt in motor_types:
                if mt.lower() in args['<path_temp>'].lower():
                    motor_type = mt

        if motor_type is None:
            print("WARNING: motor type not found")

        # create data pack files
        dp = create_data_pack(
            args['<filename>'],
            move_src,
            motor_type,
            df_temp,
            df_move)

        save_data_pack(dp)

    # Print out final summary
    if VERBOSE == 1:
        lv.summarise(df_temp)
        lv.summarise(df_move)

    # Show plots
    if VERBOSE >= 1:
        plt.show()

    print("Successfully completed processing")
