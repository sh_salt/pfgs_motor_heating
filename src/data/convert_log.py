"""Usage:
    convert_log.py <file_old> <file_new> <date>
    convert_log.py (-h | --help)

   Arguments:
    <file_old>          The log file to convert
    <file_new>          The resulting temp.csv file
    <date>              The test date yyyy\mm\dd

    Options:
    -h --help           Show this help
"""

from docopt import docopt
import re


def read_file(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()

    return lines


def formatText(lines, date):
    lines_new = []
    parse_string = "[0-9.]+|None"
    format_string = "{:02d}:{}:{} {} {},{},{},{}\n"
    # "03:34:13.425 PM 2016/08/19,21.800,0.000,0.000"

    for line in lines:
        if line.startswith('INFO Motor Temp:'):

            nums = re.findall(parse_string, line)

            if len(nums) == 6 and not (
                    nums[0] == 'None' and
                    nums[1] == 'None' and
                    nums[2] == 'None'):

                lines_new.append(format_string.format(
                    int(nums[3])-12 if int(nums[3]) > 12 else int(nums[3]),
                    nums[4],
                    nums[5],
                    'AM' if int(nums[3]) < 12 else 'PM',
                    date,
                    '' if nums[0] == 'None' else nums[0],
                    '' if nums[1] == 'None' else nums[1],
                    '' if nums[2] == 'None' else nums[2],
                ))

    return lines_new


def write_file(filename):
    with open(filename, 'w') as f:
        for line in lines:
            f.write(line)


if __name__ == '__main__':
    args = docopt(__doc__)
    # print(args)

    lines = read_file(args['<file_old>'])
    lines = formatText(lines, args['<date>'])
    write_file(args['<file_new>'])
