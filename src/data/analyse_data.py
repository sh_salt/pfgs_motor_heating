"""Usage:
    analyse_data.py <path_meer> [-vV]
    analyse_data.py (-h | --help)

   Arguments:
    <path_meer>         Input .meer.json path

   Options:
    -v --verbose        Print data summary between steps
    -V --very-verbose   Show plots and data summary between steps
    -h --help           Show this help
"""
import sys
from os import path
sys.path.append(path.join(path.dirname(__file__), '..'))

from docopt import docopt
from os import path
from toolbox import leaves as lv
from toolbox import meerkat as mk
import matplotlib.pyplot as plt
import numpy as np

_plot_num = 0
VERBOSE = 0


@lv.verbosity("Saving DataPack...")
def save_data_pack(dp):
    mk.write(dp, path.join('data', 'processed'))


@lv.verbosity("Analysing data...")
def analyse_temp(df_temp, motor_type):
    # calculate delta temperature
    first_row = \
        df_temp[df_temp['motor'].notnull() &
                df_temp['ambient'].notnull()]

    difference = df_temp['motor'] - df_temp['ambient'] - \
        first_row.loc[:, 'motor'][0] + first_row.loc[:, 'ambient'][0]

    def sum_errors(a, b):
        return np.sqrt(a**2 + b**2)

    calibration = lv.get_calibration()

    difference_error = sum_errors(df_temp['motor_error'], df_temp['ambient_error'])
    try:
        calibration_error = calibration['calibration_error'][motor_type]
    except KeyError:
        calibration_error = 0.0

    delta_error = sum_errors(difference_error, calibration_error)

    # add delta temperatures
    df_temp = df_temp.assign(
        delta=difference,
        delta_error=delta_error,
    )

    # drop raw columns
    columns_drop = ['motor', 'ambient', 'motor_error', 'ambient_error']
    df_temp = df_temp.drop(columns_drop, axis=1, errors='ignore')

    return df_temp


if __name__ == '__main__':
    args = docopt(__doc__)
    # print(args)

    # Set verbosity settings
    if args['--very-verbose']:
        VERBOSE = 2
    elif args['--verbose']:
        VERBOSE = 1

    lv.set_VERBOSE(VERBOSE)

    path_meer = args['<path_meer>']
    dp = mk.load(path_meer)

    dp.data['temp'] = analyse_temp(dp.data['temp'], dp.headers['motor_type'])
    dp = dp.trim()

    mk.write(dp, path.join('data', 'analysed'))

    # Print out final summary
    if VERBOSE >= 1:
        for key, df in dp.data.items():
            lv.summarise(df)
        plt.show()

    print("Successfully completed analysis")
